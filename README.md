# ConDeFidential #



### What is ConDeFidential? ###

**ConDeFidential** is a privacy blockchain for decentralized financial accounts that hides token transactions.

Account addresses & balances and token transfers between accounts are rendered confidential.